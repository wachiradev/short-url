<?php

namespace App\Models;

use CodeIgniter\Model;

class Log extends Model
{
    //protected $DBGroup          = 'default';
    protected $table            = 'logs';
    protected $primaryKey       = 'id';
    protected $allowedFields    = [
        'src_url', 'shorten', 'ip', 'created_at'
    ];
}
