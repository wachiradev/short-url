<!DOCTYPE html>
<html lang="en">
<head>
  <title>URL Shortener - Short URL</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" 
  rel="stylesheet">

  <meta name="<?=csrf_token()?>" content="<?=csrf_hash()?>">

</head>
<body class="bg-dark">

<div class="container-fluid p-5 bg-primary text-white text-center">
  <h1>Short URL</h1> 
</div>
  
<div class="container mt-5">
  <div class="row">
        <div class="col-md-12">
            <div class="input-group">
                <input type="text" name="src_url" class="form-control" placeholder="Shorten your link" autocomplete="off" >
                <button class="btn btn-primary" id="btnShorten">Shorten</button>
            </div>
        </div>
        <div class="col-md-12 text-danger pt-2" id="msg">
            
        </div>
  </div>
  <div class="row">
        <div class="col-md-12 mt-5">
            <div class="bg-light">
               <div class="row p-3">
                    <h5>History shorten link</h5>
                    <div class="pt-3" id="logs">

                    </div>
               </div>
            </div>
        </div>
  </div>
</div>


<script src="https://code.jquery.com/jquery-3.6.0.js"></script>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js"></script>
<script src="<?=base_url('js/qrcodejs/qrcode.js')?>"></script>

<script>
    $(document).ready(function(){
        var baseURL     = '<?=base_url()?>';
        var $btnShorten = $('#btnShorten');
        var $logEl      = $('#logs');

        $btnShorten.off('click.shorten').on('click.shorten', function(e){

            var $srcUrl = $('[name="src_url"]');
            var srcUrl  =  $srcUrl.val() || '';

            if( !isValidURL(srcUrl) ){
                $('#msg').empty().html('Invalid your URL.');
                return;
            }

            $.ajax({
                type: 'POST',
                url : baseURL + '/logs/add',
                data: {
                    src_url   : srcUrl
                },
                dataType: 'json',
                success : function(data){
                    if( data.success === true ){
                        setTimeout(() => {
                            $srcUrl.val(data.message);
                            loadLog();
                        }, 0);
                    }
                },
                error   : function(xhr){
                    console.log( 'xhr => ', xhr.statusText )
                }
            });
        });

        loadLog();
        function loadLog(){
            $.ajax({
                type: 'POST',
                url : baseURL + '/logs/load',
                dataType: 'json',
                success : function(data){
                   if( data.data.length > 0 ){
                      var html = ``;
                      var rows = data.data;

                      $logEl.empty();

                      for(let i = 0; i < rows.length; i++){

                        html = `
                            <div class="row mt-2">
                                <div class="col-md-5">${rows[i].src_url}</div>
                                <div class="col-md-5">
                                    <a href="${baseURL}/${rows[i].shorten}" title="${rows[i].src_url}" class="open-web" target="_blank">${baseURL}/${rows[i].shorten}</a>
                                </div>
                                <div class="col-md-2">
                                    <div id="qrCode_${i}"></div>
                                </div>
                            </div>
                        `;
                        
                        $logEl.append(html);

                        new QRCode(document.getElementById('qrCode_'+ i), {
                            text         : baseURL + '/' + rows[i].shorten,
                            width        : 100,
                            height       : 100,
                            colorDark    : "#000000",
                            colorLight   : "#ffffff",
                            correctLevel : QRCode.CorrectLevel.H
                        });
                      }

                   }
                },
                error   : function(xhr){
                    console.log( 'xhr => ', xhr.statusText )
                }
            });
        }


        $(document).on('click', '.open-web', function(e){
            e.preventDefault();
            var link = $(this).attr('title');
            if (link.indexOf('http://') == 0 || link.indexOf('https://') == 0) {
                window.open(link);
                return;
            }
            window.open('http://' + link);
        })

        function isValidURL(string) {
            var res = string.match(/(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/g);
            return (res !== null)
        };
    });
</script>
</body>
</html>
