<?php

namespace App\Controllers;

use App\Controllers\BaseController;

use App\Models\Log;

class Logs extends BaseController
{
    public function add(){
        
        $params               = $this->request->getPost();
        $params['shorten']    = $this->generateCode();
        $params['ip']         = $this->getIp();
        $params['created_at'] = date('Y-m-d H:i:s');

        try{
            $model = new Log();
            
            $save  = $model->save($params);
            
            if( $save ){
                return $this->response->setJSON([
                    'success' => true,
                    'message' => base_url().$params['shorten']
                ]);
            }else{
                return $this->response->setJSON([
                    'success' => false,
                    'message' => 'Generate shorten link failed.'
                ]);
            }
        } catch ( Exception $e){
            return $this->response->setJSON([
                'success' => false,
                'message' => $e->getMessage()
            ]);
        }
    }

    private function generateCode($length = 10)
    {
        $characters       = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString     = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function load(){
        $model = new Log();
        $ip    = $this->getIp();
        $data  = $model->where('ip', $ip)->orderBy('created_at', 'desc')->findAll();
        return $this->response->setJSON(['data' => $data]);
    }


    public function getIp(){
        return $this->request->getIPAddress();
    }


    public function goto($id = null){
        $model = new Log();

        $data  = $model->select('src_url')->where('shorten', $id)->first();

        header("Cache-Control: no-cache");
        header("Pragma: no-cache");
        header('Location: '.$data['src_url']);
        die();
    }
}
