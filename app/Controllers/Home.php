<?php

namespace App\Controllers;

use App\Models\Log;

class Home extends BaseController
{
    
    public function index( $id  = null )
    {
        $model = new Log();

        $data  = $model->select('src_url')->where('shorten', $id)->first();

        if( !empty($data['src_url']) ){
            $uri = $data['src_url'];
            if( substr($uri, 0, 7) === 'http://' || substr($uri, 0, 8) === 'https://'){
                header('Location:'.$data['src_url']);
            }else{
                header('Location:http://'.$data['src_url']);
            }
            exit;
        }

        return view('index');
    }

}
